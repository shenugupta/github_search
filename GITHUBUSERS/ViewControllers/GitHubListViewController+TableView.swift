//
//  GitHubListViewController+TableView.swift
//  GITHUBUSERS
//
//  Created by Shenu Gupta on 2018-08-15.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit

extension GitHubListViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: tableview delegates and datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let users = users {
            return users.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! CustumGitUserTableViewCell
        let getGITDetail: GitHubUsersDetails = self.users![indexPath.row]
        cell.userLogin.text = getGITDetail.login
        if let url = getGITDetail.avatar_url {
            let image = UIImage(named: "Placeholder Image.png")
            cell.configureCell(with: url, placeholderImage:image!)
        }
        else{
            print("image is nil")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if let getGitUserDetail: GitHubUsersDetails = self.users?[indexPath.row] {
           self.performSegue(withIdentifier: userDetailSeque, sender: getGitUserDetail)
        }
    }
}
