//
//  GitHubListViewController.swift
//  GITHUBUSERS
//
//  Created by Shenu Gupta on 8/11/18.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
class GitHubListViewController: UIViewController {
    var followerViewModel = FollowersModel()
    @IBOutlet weak var gitUserTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    let viewModel = GitHubUsersModel()
    var users: [GitHubUsersDetails]?
    let cellReuseIdentifier: String = "gitUserIdentifier";
    let userDetailSeque: String = "gitUserDetail";
    lazy var placeholderImage: UIImage = {
        let image = UIImage(named: "Placeholder Image")!
        return image
    }()
    
    @objc func getGitHubUserData(searchText: String) {
        if NetworkConnectivity.isConnectedToInternet() {
            if let searchBarText = searchBar.text {
                viewModel.getSearchResults(searchText: searchBarText) { results, errorMessage in
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    if let results = results {
                        self.users = results
                        self.gitUserTableView.reloadData()
                    }
                    if !errorMessage.isEmpty { print("Search error: " + errorMessage) }
                }
            }
        }
        else {
            users =  viewModel.fetchFromStorage(searchText: searchText)
            if users != nil {
                gitUserTableView.reloadData()
            }
            else {
                print("no records found in offline database")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == userDetailSeque) {
            let vc = segue.destination as! UserDetailsViewController
           vc.githubUser = sender as! GitHubUsersDetails
        }
    }
}


