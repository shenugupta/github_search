//
//  UserFollowersViewController.swift
//  GITHUBUSERS
//
//  Created by Shenu Gupta on 2018-08-15.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit

class UserFollowersViewController: UICollectionViewController {
    var followers = [Follower]()
    let cellReuseIdentifier: String = "followerReuseCellIdentifier";
}
