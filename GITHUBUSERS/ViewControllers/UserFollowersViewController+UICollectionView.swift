//
//  UserFollowersViewController+UICollectionView.swift
//  GITHUBUSERS
//
//  Created by Shenu Gupta on 2018-08-15.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit

extension UserFollowersViewController {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return followers.count
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! FollowersCollectionViewCell
        let follower: Follower = self.followers[indexPath.row]
        cell.name.text = follower.login
        if let url = follower.avatar_url {
            let image = UIImage(named: "Placeholder Image.png")
            cell.configureCell(with: url, placeholderImage:image!)
        }
        else{
            print("image is nil")
        }
        return cell
    }
}
