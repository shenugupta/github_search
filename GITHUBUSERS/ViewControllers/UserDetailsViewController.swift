//
//  UserDetailsViewController.swift
//  GITHUBUSERS
//
//  Created by Shenu Gupta on 8/13/18.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit

class UserDetailsViewController: UITableViewController {
    
    var githubUser: GitHubUsersDetails!
    
    @IBOutlet weak var followersActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var login: UILabel!
    @IBOutlet weak var followersCount: UILabel!
    @IBOutlet weak var followersCell: UITableViewCell!
    @IBOutlet weak var publicRepos: UITableViewCell!
    
    var followers = [Follower]()
    var repos = [PublicRepos]()
    let viewModel = FollowersModel()
    let showFollowersVcSegue = "showFollowersVcSegue"
    let publicReposSeque = "publicReposSeque"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.followersActivityIndicator.startAnimating()
        if let avatar_url = githubUser.avatar_url {
            avatar.load(urlString: avatar_url)
        }
        if let name = githubUser.login {
            login.text = name
        }
        getFollowers()
    }
    
    func getFollowers() {
        if NetworkConnectivity.isConnectedToInternet() {
            if let followers_url = githubUser.followers_url {
                viewModel.getFollowerData(followerUrl: followers_url) { results, errorMessage in
                    self.followersActivityIndicator.stopAnimating()
                    if let results = results {
                        self.followers = results
                        self.followersCount.text = String(self.followers.count)
                    }
                    else {
                        self.followersCount.text = "Error: \(errorMessage)"
                    }
                }
            }
        }
        else {
            print("internet is not working. followers information is not saved offline.")
        }
    }
    
    func getPublicRepos() {
        if NetworkConnectivity.isConnectedToInternet() {
            if let reposUrl = githubUser.repos_url {
                viewModel.getReposInfo(repoUrl: reposUrl) { results, errorMessage in
                    if let results = results {
                        self.repos = results
                        if (self.repos.count > 0) {
                            self.performSegue(withIdentifier: self.publicReposSeque, sender: self.repos)
                        }
                    }
                    else {
                        self.followersCount.text = "Error: \(errorMessage)"
                    }
                }
            }
        }
        else {
            print("internet is not working. followers information is not saved offline.")
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let staticIndexPath = tableView.indexPath(for: followersCell), staticIndexPath == indexPath {
            if (followers.count > 0) {
              self.performSegue(withIdentifier: showFollowersVcSegue, sender: followers)
            }
        }
        if let staticIndexPath = tableView.indexPath(for: publicRepos), staticIndexPath == indexPath {
            getPublicRepos()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == showFollowersVcSegue {
            let vc = segue.destination as! UserFollowersViewController
            vc.followers = followers
            if let name = githubUser.login {
               vc.title = "Followers of \(String(describing: name))"
            }
        }
        if segue.identifier == publicReposSeque {
            let vc = segue.destination as! PublicReposTableView
            vc.repos = repos
            if let name = githubUser.login {
                vc.title = "Public Repos of \(String(describing: name))"
            }
        }
    }
}
