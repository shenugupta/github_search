//
//  PublicReposTableView+TableView.swift
//  GITHUBUSERS
//
//  Created by Shenu Gupta on 2018-08-16.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit

extension PublicReposTableView {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repos.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: publicRepoReuseIdentifier, for: indexPath) as! PublicRepoCell
        let repo: PublicRepos = self.repos[indexPath.row]
        cell.name.text = repo.name
        cell.repoDescription.text = repo.description
        cell.updatedAt.text = repo.updated_at
        return cell
    }
}
