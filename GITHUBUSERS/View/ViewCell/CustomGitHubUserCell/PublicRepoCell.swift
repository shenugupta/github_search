//
//  PublicRepoCell.swift
//  GITHUBUSERS
//
//  Created by Shenu Gupta on 2018-08-16.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit

class PublicRepoCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var repoDescription: UILabel!
    @IBOutlet weak var updatedAt: UILabel!
}
