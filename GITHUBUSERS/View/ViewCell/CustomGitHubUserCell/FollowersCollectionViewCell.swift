//
//  FollowersCollectionViewCell.swift
//  GITHUBUSERS
//
//  Created by Shenu Gupta on 2018-08-15.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class FollowersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    func configureCell(with URLString: String, placeholderImage: UIImage) {
        Alamofire.request(URLString).responseImage { response in
            debugPrint(response)
            if let image = response.result.value {
                self.avatar.image = image
            }
        }
    }
}
