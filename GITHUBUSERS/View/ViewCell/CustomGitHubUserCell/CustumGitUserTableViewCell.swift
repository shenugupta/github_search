//
//  CustumGitUserTableViewCell.swift
//  GITHUBUSERS
//
//  Created by Shenu Gupta on 8/11/18.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class CustumGitUserTableViewCell: UITableViewCell {
    @IBOutlet weak var userLogin: UILabel!
    @IBOutlet weak var profileImgView: UIImageView!
    
    func configureCell(with URLString: String, placeholderImage: UIImage) {
        Alamofire.request(URLString).responseImage { response in
            debugPrint(response)
            if let image = response.result.value {
                self.profileImgView.image = image
            }
        }
    }
}
