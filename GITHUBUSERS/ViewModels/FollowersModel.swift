//
//  FollowersModel.swift
//  GITHUBUSERS
//
//  Created by Shenu Gupta on 8/11/18.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import Foundation

enum FollwersApiParsingError: Error {
    case invalidUrl
    case invalidEncoding
    case serverError
}

class FollowersModel {
    
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    typealias FollowersResult = ([Follower]?, String) -> ()
    typealias PublicReposResult = ([PublicRepos]?, String) -> ()
    var errorMessage = ""
    
    func getFollowerData(followerUrl: String, completion: @escaping FollowersResult) {
        var followers:[Follower]?
        dataTask?.cancel()
        if var urlComponents = URLComponents(string: followerUrl) {
            guard let url = urlComponents.url else { return }
            dataTask = defaultSession.dataTask(with: url) { data, response, error in
                defer { self.dataTask = nil }
                if let error = error {
                    self.errorMessage += "DataTask error: " + error.localizedDescription
                    DispatchQueue.main.async {
                        completion(nil, self.errorMessage)
                    }
                } else if let data = data,
                    let response = response as? HTTPURLResponse,
                    response.statusCode == 200 {
                    followers = self.parseFollowersData(data: data)
                    DispatchQueue.main.async {
                        completion(followers, self.errorMessage)
                    }
                }
            }
            dataTask?.resume()
        }
    }
    
    func getReposInfo(repoUrl: String, completion: @escaping PublicReposResult) {
        var repos:[PublicRepos]?
        dataTask?.cancel()
        if var urlComponents = URLComponents(string: repoUrl) {
            guard let url = urlComponents.url else { return }
            dataTask = defaultSession.dataTask(with: url) { data, response, error in
                defer { self.dataTask = nil }
                if let error = error {
                    self.errorMessage += "DataTask error: " + error.localizedDescription
                    DispatchQueue.main.async {
                        completion(nil, self.errorMessage)
                    }
                } else if let data = data,
                    let response = response as? HTTPURLResponse,
                    response.statusCode == 200 {
                    repos = self.parseReposData(data: data)
                    DispatchQueue.main.async {
                        completion(repos, self.errorMessage)
                    }
                }
            }
            dataTask?.resume()
        }
    }
    
    func parseFollowersData(data: Data) -> [Follower] {
        do {
            let decoder = JSONDecoder()
            return try decoder.decode([Follower].self, from: data)
        }
        catch {
            fatalError("Failed to retrieve managed object context")
        }
    }
    
    func parseReposData(data: Data) -> [PublicRepos] {
        do {
            let decoder = JSONDecoder()
            return try decoder.decode([PublicRepos].self, from: data)
        }
        catch {
            fatalError("Failed to retrieve managed object context")
        }
    }
}
