//
//  GitHubUsersModel.swift
//  GITHUBUSERS
//
//  Created by Shenu Gupta on 8/11/18.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit
import CoreData

class GitHubUsersModel {
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    typealias GithubUsersSearchResult = ([GitHubUsersDetails]?, String) -> ()
    var errorMessage = ""
    let managedObjectContext:NSManagedObjectContext!
    
    init() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        managedObjectContext = appDelegate.persistentContainer.viewContext
    }
    
    func getSearchResults(searchText: String, completion: @escaping GithubUsersSearchResult) {
        var gitHubUsers:[GitHubUsersDetails]?
        dataTask?.cancel()
        if var urlComponents = URLComponents(string: "https://api.github.com/search/users") {
            urlComponents.query = "q=" + searchText + "&page=1"
            guard let url = urlComponents.url else { return }
            dataTask = defaultSession.dataTask(with: url) { data, response, error in
                defer { self.dataTask = nil }
                if let error = error {
                    self.errorMessage += "DataTask error: " + error.localizedDescription
                    DispatchQueue.main.async {
                        completion(nil, self.errorMessage)
                    }
                } else if let data = data,
                    let response = response as? HTTPURLResponse,
                    response.statusCode == 200 {
                    gitHubUsers = self.parseSearchResults(data: data)
                    DispatchQueue.main.async {
                        completion(gitHubUsers, self.errorMessage)
                    }
                }
            }
            dataTask?.resume()
        }
    }
    
    func parseSearchResults(data: Data) -> [GitHubUsersDetails]  {
        var gitHubUsers = [GitHubUsersDetails]()
        do {
            guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext else {
                fatalError("Failed to retrieve managed object context")
            }
            let decoder = JSONDecoder()
            decoder.userInfo[codingUserInfoKeyManagedObjectContext] = managedObjectContext
            let user: GitHubUser? = try decoder.decode(GitHubUser.self,  from: data)
            try managedObjectContext.save()
            if ((user?.items?.count) != nil) {
                gitHubUsers = (user?.items)!
            }
        } catch let error {
            print(error)
        }
        return gitHubUsers;
    }
    
    func fetchFromStorage(searchText: String) -> [GitHubUsersDetails]? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<GitHubUsersDetails>(entityName: "GitHubUsersDetails")
        let predicate = NSPredicate(format: "login contains[c] %@", searchText)
        fetchRequest.predicate = predicate
        do {
            return try managedObjectContext.fetch(fetchRequest)
        } catch let error {
            print(error)
            return nil
        }
    }
}

