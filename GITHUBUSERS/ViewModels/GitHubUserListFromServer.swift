//
//  GitHubUserListFromServer.swift
//  GITHUBUSERS
//
//  Created by Shenu Gupta on 8/12/18.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit

enum GitHubListApiParsingError: Error {
    case invalidUrl
    case invalidEncoding
    case serverError
}

class GitHubListModell{
    let gitHubInfoUrl = "https://api.github.com/search/users?q=torvalds&page=1"
    var reloadList = {()->() in}
    var errorMessage = {(message:String) ->() in}
    var gitUsers:GitHubUser1? = nil {
        didSet{
            reloadList()
        }
    }
    
    func getGitHubListFromServer() throws {
        guard let listURL = URL(string: gitHubInfoUrl) else {
            throw GitHubListApiParsingError.invalidUrl
        }
        URLSession.shared.dataTask(with:listURL) {
            (data, response, error) in
            let data = data
            do {
                self.gitUsers = try self.parseData(data: data!);
            } catch let error as NSError {
                print("error \(error)")
            }
            }.resume()
    }
    
    func parseData(data: Data)throws -> GitHubUser1 {
        do {
            guard let isoLatin1String = String(data: data, encoding: .utf8) as String? else {
                throw FollwersApiParsingError.invalidEncoding
            }
            let dataFromLatin1String = Data(isoLatin1String.utf8)
            let decoder = JSONDecoder()
            return try decoder.decode([GitHubUser1].self, from: dataFromLatin1String)
        }
        catch {
            throw FollwersApiParsingError.serverError
        }
    }
}
