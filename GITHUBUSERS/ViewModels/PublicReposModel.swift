//
//  PublicReposModel.swift
//  GITHUBUSERS
//
//  Created by Shenu Gupta on 2018-08-15.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import UIKit

enum PublicReposApiParsingError: Error {
    case invalidUrl
    case invalidEncoding
    case serverError
}

class PublicReposModel{
    let gitHubInfoUrl = "https://api.github.com/search/users?q=torvalds&page=1"
    var reloadList = {()->() in}
    var errorMessage = {(message:String) ->() in}
    var publicRepos:[PublicRepos]? = nil {
        didSet{
            reloadList()
        }
    }
    
    func getFollowerData(followerUrl: String) throws {
        guard let listURL = URL(string: followerUrl) else {
            throw PublicReposApiParsingError.invalidUrl
        }
        URLSession.shared.dataTask(with:listURL) {
            (data, response, error) in
            let data = data
            do {
                self.publicRepos = try self.parseData(data: data!);
            } catch let error as NSError {
                print("error \(error)")
            }
            }.resume()
    }
    
    func parseData(data: Data)throws -> [PublicRepos] {
        do {
            guard let isoLatin1String = String(data: data, encoding: .utf8) as String? else {
                throw PublicReposApiParsingError.invalidEncoding
            }
            let dataFromLatin1String = Data(isoLatin1String.utf8)
            let decoder = JSONDecoder()
            return try decoder.decode([PublicRepos].self, from: dataFromLatin1String)
        }
        catch {
            throw PublicReposApiParsingError.serverError
        }
    }
}

