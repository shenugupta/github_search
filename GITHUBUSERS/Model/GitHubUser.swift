//
//  GitHubUser.swift
//  GITHUBUSERS
//
//  Created by Shenu Gupta on 8/11/18.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import Foundation
import CoreData

class GitHubUser: NSManagedObject, Codable {
    
    enum CodingKeys: String, CodingKey {
        case items
    }
    var items : [GitHubUsersDetails]?
    
    required convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "GitHubUser", in: managedObjectContext) else {
                fatalError("Failed to decode User")
        }
        self.init(entity: entity, insertInto: managedObjectContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.items = try container.decodeIfPresent([GitHubUsersDetails].self, forKey: .items)
    }
}

class GitHubUsersDetails: NSManagedObject, Codable {
    
    enum CodingKeys: String, CodingKey {
       case login
       case id
       case node_id
       case avatar_url
       case gravatar_id
       case url
       case html_url
       case followers_url
       case following_url
       case gists_urll
       case starred_url
       case subscriptions_url
       case organizations_url
       case repos_url
       case events_url
       case received_events_url
       case type
       case site_admin
       case score
    }
      @NSManaged var login : String?
      //@NSManaged var id : NSNumber?
      @NSManaged var node_id : String?
      @NSManaged var avatar_url : String?
      @NSManaged var gravatar_id : String?
      @NSManaged var url : String?
      @NSManaged var html_url : String?
      @NSManaged var followers_url : String?
      @NSManaged var following_url : String?
      @NSManaged var gists_urll : String?
      @NSManaged var starred_url : String?
      @NSManaged var subscriptions_url : String?
      @NSManaged var organizations_url : String?
      @NSManaged var repos_url : String?
      @NSManaged var events_url : String?
      @NSManaged var received_events_url : String?
      @NSManaged var type : String?
      //@NSManaged var site_admin : NSNumber?
      //@NSManaged var score : NSNumber?
    
    required convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "GitHubUsersDetails", in: managedObjectContext) else {
                fatalError("Failed to decode User")
        }
        self.init(entity: entity, insertInto: managedObjectContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.login = try container.decodeIfPresent(String.self, forKey: .login)
        //self.id = try container.decodeIfPresent(Int.self, forKey: .id) as NSNumber?
        self.node_id = try container.decodeIfPresent(String.self, forKey: .node_id)
        self.avatar_url = try container.decodeIfPresent(String.self, forKey: .avatar_url)
        self.gravatar_id = try container.decodeIfPresent(String.self, forKey: .gravatar_id)
        self.url = try container.decodeIfPresent(String.self, forKey: .url)
        self.html_url = try container.decodeIfPresent(String.self, forKey: .html_url)
        self.followers_url = try container.decodeIfPresent(String.self, forKey: .followers_url)
        self.following_url = try container.decodeIfPresent(String.self, forKey: .following_url)
        self.gists_urll = try container.decodeIfPresent(String.self, forKey: .gists_urll)
        self.starred_url = try container.decodeIfPresent(String.self, forKey: .starred_url)
        self.subscriptions_url = try container.decodeIfPresent(String.self, forKey: .subscriptions_url)
        self.organizations_url = try container.decodeIfPresent(String.self, forKey: .organizations_url)
        self.repos_url = try container.decodeIfPresent(String.self, forKey: .repos_url)
        self.events_url = try container.decodeIfPresent(String.self, forKey: .events_url)
        self.received_events_url = try container.decodeIfPresent(String.self, forKey: .received_events_url)
        self.type = try container.decodeIfPresent(String.self, forKey: .type)
        //self.site_admin = try container.decodeIfPresent(Int.self, forKey: .site_admin) as NSNumber?
        //self.score = try container.decodeIfPresent(Int.self, forKey: .score) as NSNumber?
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(login, forKey: .login)
        //try container.encode(id, forKey: .id)
        try container.encode(node_id, forKey: .node_id)
        try container.encode(avatar_url, forKey: .avatar_url)
        try container.encode(gravatar_id, forKey: .gravatar_id)
        try container.encode(url, forKey: .url)
        try container.encode(html_url, forKey: .html_url)
        try container.encode(followers_url, forKey: .followers_url)
        try container.encode(following_url, forKey: .following_url)
        try container.encode(gists_urll, forKey: .gists_urll)
        try container.encode(starred_url, forKey: .starred_url)
        try container.encode(subscriptions_url, forKey: .subscriptions_url)
        try container.encode(organizations_url, forKey: .organizations_url)
        try container.encode(repos_url, forKey: .repos_url)
        try container.encode(events_url, forKey: .events_url)
        try container.encode(received_events_url, forKey: .received_events_url)
        try container.encode(type, forKey: .type)
        //try container.encode(site_admin, forKey: .site_admin)
        //try container.encode(score, forKey: .score)
    }
}
