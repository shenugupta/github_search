//
//  PublicRepos.swift
//  GITHUBUSERS
//
//  Created by Shenu Gupta on 2018-08-15.
//  Copyright © 2018 Shenu Gupta. All rights reserved.
//

import Foundation

struct PublicRepos : Codable {
    let name : String?
    let description : String?
    let updated_at : String?
}
